#!/bin/bash

#ansible paths ( user configurable)
ANSIBLE_DATA=~/projects/ansible_project
ANSIBLE_ROOT=~/projects/ansible
#ANSIBLE_BIN_PATH=$ANSIBLE_ROOT/bin
#ANSIBLE_PLAYBOOK_BIN_PATH=$ANSIBLE_BIN_PATH/ansible-playbook 
#ANSIBLE_ADHOC_BIN_PATH=$ANSIBLE_BIN_PATH/ansible
ANSIBLE_INVENTORY_DYNAMIC=~/.ansible/collections/ansible_collections/community/digitalocean/scripts/inventory/digital_ocean.py

#tags
DEFAULT_TAG="--skip-tags delete"

#playbook paths with default tags
PROVISION_PLAYBOOK="$ANSIBLE_DATA/provision.yml $DEFAULT_TAG"
PROVISION_PLAYBOOK_DELETE=$ANSIBLE_DATA/provision.yml 
GETHOSTS_PLAYBOOK=$ANSIBLE_DATA/gethosts.yml



#set the default command
command="ansible-playbook -u root -i $ANSIBLE_INVENTORY_DYNAMIC"

setcommandoptions()
{
	#adds options to the command string 

	if [ "$ANSIBLE_DEBUG" == "n" ]; then
		command="$command --skip-tags debug"
	fi

	if [ "$ANSIBLE_VERBOSITY" != "" ]; then
		command="$command $ANSIBLE_VERBOSITY"
	fi	

	if [ "$ANSIBLE_LIMIT" != "" ]; then
		command="$command --limit $ANSIBLE_LIMIT"
	fi

	if [ "$ANSIBLE_SYNTAXCHECK" == "y" ]; then
		command="$command --syntax-check"
	fi
		
}


do_fetchfile()
{

	   read -p "What file/directory do you like to fetch? - please include full path" fullpathsrc 

	   command="$ANSIBLE_ADHOC_BIN_PATH -u root -i $ANSIBLE_INVENTORY_DYNAMIC"
}


#####################action function 
process_actions()
{
    act=$1     

	#case for playbook
    case $act in 
       db) command="$command $PROVISION_PLAYBOOK --tags database";;
       ms) command="$command $PROVISION_PLAYBOOK --tags mysql";;
	   dms) command="$command $PROVISION_PLAYBOOK_DELETE --tags mysql_delete";;
	   dp) command="$command $PROVISION_PLAYBOOK";;
       gh) command="$command $GETHOSTS_PLAYBOOK";;
	   fetch) do_getfile;;
	   *)	echo "error" 
	   exit;;
esac

}


#####################options 

#read the options
while getopts "Dv:l:s" flag 
do
	case $flag	in
		D)	ANSIBLE_DEBUG="y";	
			;;
		v)	
			if [ $OPTARG -eq 1 ];	
			then
				ANSIBLE_VERBOSITY="-v";
			fi 
			if [ $OPTARG -eq 2 ];	
			then
				ANSIBLE_VERBOSITY="-vv";
			fi
			if [ $OPTARG -eq 3 ];	
			then
				ANSIBLE_VERBOSITY="-vvv";
			fi
			if [ $OPTARG -eq 4 ];	
			then
				ANSIBLE_VERBOSITY="-vvvv";
			fi
			if [ $OPTARG -eq 5 ];	
			then
				ANSIBLE_VERBOSITY="-vvvvv";
			fi
			;;
		l)	
			ANSIBLE_LIMIT=$OPTARG
			;;
		s)  ANSIBLE_SYNTAXCHECK="y";
		    ;;
	esac
done


#setup the options
setcommandoptions

#print playbook you like to run
echo    "db --  database provision:"
echo    "ms --  mysql(mariadb) provision:"
echo    "dms -- delete mysql and any other database modules:"
echo    "gh --  gethosts provision:"
echo    "dp --  droplet provision:"
read -p "What do you want to do? " action
process_actions "$action"

#setup the ansible env
#cd $ANSIBLE_ROOT

#if [ "$ANSIBLE_VERBOSITY" == "" ]; then
#	source ./hacking/env-setup -q
#else
#	source ./hacking/env-setup 
#fi

#echo $command
$command
